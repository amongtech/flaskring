from flask import Flask, escape, request, g, redirect, render_template
from markupsafe import Markup
import json, re, random

# initialize Flask
app = Flask("flaskring")

# load sites JSON into a dict for later usage
with open("sites.json") as f:
    file = f.read()
sites = json.loads(file)
sites = sites["sites"]

# render homepage on / URI
@app.route("/")
def homepage():
    return render_template('ringhome.html', sites=sites)

@app.route("/prev/<host>")
def prev(host=None):
    found, i = False, 0
    # check through sites to find if the passed url is in the sites list
    for site in sites:
        if (site["url"] == host):
            found = True
            break
        i = i + 1
    # abort the program if an invalid url is given, or give previous redirect
    if (found == False):
        return render_template('error.html', text="URL not in sites list passed in URL")
    else:
        if (i == 0):
            i = len(sites) - 1
        else:
            i = i - 1
        found = sites[i]
        return redirect("https://" + found["url"])

@app.route("/next/<host>")
def next(host):
    found, i = False, 0
    # check through sites to find if the passed url is in the sites list
    for site in sites:
        if (site["url"] == host):
            found = True
            break
        i = i + 1
    # abort the program if an invalid url is given, or give previous redirect
    if (found == False):
        return render_template('error.html', text="URL not in sites list passed in URL")
    else:
        if (i == len(sites) - 1):
            i = 0
        else:
            i = i + 1
        found = sites[i]
        return redirect("https://" + found["url"])

@app.route("/random")
def randomsite():
        num = random.randrange(0, len(sites), 1)
        site = sites[num]
        return redirect("https://" + site["url"])

app.run('127.0.0.1', 5013)
