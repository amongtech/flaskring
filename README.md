# Flaskring

A simple webring backend made with Flask.

## How to set up
Install the following dependencies:
```plain
python3
python3-pip
git
```
Then, run the following commands:
```sh
pip install flask

# Clone the repo
git clone https://codeberg.org/amongtech/flaskring
cd flaskring

# Edit these files as needed
cp flaskring/templates/error.example.html flaskring/templates/error.html
cp flaskring/templates/ringhome.example.html flaskring/templates/ringhome.html
cp sites.example.json sites.json

# Once done, run the run command
FLASK_APP=flaskring flask run
```
## Configuration

### Static assets

To make static assets, run the command:
```sh
mkdir static
```
and place all static assets in there. In your files, reference them with `static/file.exs`.

### `ringhome.html`

`ringhome.html` is the homepage for your webring. You may use any valid HTML, CSS, JS, anything, but if you want your links to show on the homepage you must include a snippet like so: (the following snippet uses an unordered list to show all URLS, you can use anything):
```html
<ul>
{% for site in sites %}
	<li><a href="https://{{ site.url }}">{{ site.name }}</a></li>
{% endfor %}
</ul>
```

### `error.html`

`error.html` is loaded whenever there is an error with the request. The only required template message (to show the error) is `{{ text }}`.

### `sites.json`

A typical `sites.json` config would look as follows:
```json
{ "sites": [
	{
		"name": "Example",
		"url": "example.com"
	},
	{
		"name": "Example 2",
		"url": "two.example.com"
	}
]}
```
These names are loaded into the index page, and the urls must not have an HTTP/HTTPS protocol indicator before them as Flaskring adds them.

## Contributions are welcome!
